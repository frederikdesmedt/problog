import sys
from time import time
from shutil import copyfile
import json

import pyspark

import problog.learning.dlfi as dlfi
from .dlfi import ExampleSet, SparkRunner, DLFIProblem

from .lfi import LFIProblem
from problog.program import PrologFile

def to_exampleset(examples):
    """
    Converts a list of examples to an ExampleSet of examples.
    :param examples: A list of examples
    :type examples: [[(Term, Bool)]]
    :return: An ExampleSet that contains every example in the examples list
    :rtype: ExampleSet
    """
    exampleset = ExampleSet()
    for index, example in enumerate(examples):
        atoms, values = zip(*example)
        exampleset.add(index, atoms, values)
    return exampleset

def perform_dlfi_compilation(runner, program, examples):
    problem = DLFIProblem(program, examples)
    iter(problem)
    num_parameters = problem.count
    runner.general_problem = problem
    def prepare(dlfi):
        if dlfi is None:
            return 0
        else:
            start = time()
            dlfi.prepare()
            end = time()
            return end - start
    return runner.context.parallelize(range(0, num_parameters), num_parameters)\
                        .map(lambda i: prepare(runner.create_single_parametered(i)))\
                        .sum()


def benchmark_dlfi_compilation(program, examples, sparkContext, output, params):
    """
    Compiles all examples with the specified source in both local LFI and Spark LFI and times it.

    :param program: The file that contains the model that should be used for compilation
    :type program: BenchmarkPrologFile
    :param examples: A list of all the examples that should be compiled
    :type examples: [[(Term, Bool)]]
    :param sparkContext: An active SparkContext to connect to the Spark master
    :type sparkContext: SparkContext
    :param output: The file on which the benchmark results should be stored
    :type output: string
    """
    runner = SparkRunner(sparkContext)
    exampleset = to_exampleset(examples)
    spark_time = perform_dlfi_compilation(runner, program, exampleset)
    lfi = LFIProblem(program, examples)
    start_local_time = time()
    lfi.prepare()
    end_local_time = time()
    with open(output, 'a+') as out:
        out.write('{}\n'.format(json.dumps({'type': 'compilation',
                                'local': end_local_time - start_local_time,
                                'spark': spark_time,
                                'num_examples': len(examples),
                                'program': program.source,
                                'num_unknowns': lfi.count})))

def benchmark_dlfi_number_iterations(program, examples, sparkContext, output, params,
    spark_max_iterations=1e6, spark_min_improv=1e-10, record_time=False, record_iterations=False,
    dlfi_kwargs=dict(), lfi_kwargs=dict()):
    """
    Counts the number of iterations that occur in DLFIProblem compared to LFIProblem
    to test the performance of DLFIProblem.

    :param program: The file that contains the model of which the parameters should be learned.
    :type program: BenchmarkPrologFile
    :param examples: A list of all the examples DLFIProblem should learn from
    :type examples: [[(Term, Bool)]]
    :param sparkContext: An active SparkContext to connect to the Spark master
    :type sparkContext: SparkContext
    :param output: The file on which the benchmark results should be stored
    :type output: string
    :param spark_max_iterations: The maximum number of global iterations that should be performed in the Spark-implementation
    :type spark_max_iterations: float
    :param spark_min_improv: The minimum improvement that must be observed by the global Spark-loop before another iteration is performed
    :type spark_min_improv: float
    :param record_time: Whether the timing should be logged to the output
    :type record_time: bool
    :param record_iterations: Whether the total number of iterations should be logged to the output
    :type record_iterations: bool
    """
    # Prepare the DLFIProblem and LFIProblem for the benchmark.
    exampleset = to_exampleset(examples)
    lfi = LFIProblem(program, examples, **lfi_kwargs)
    start_local_time = time()
    lfi_score = lfi.run()
    end_local_time = time()
    print('Completed local: {}'.format(lfi._weights))
    start_time = time()
    runner = SparkRunner(sparkContext, max_iterations=spark_max_iterations, min_improv=spark_min_improv)
    runner.compile(exampleset, program, **dlfi_kwargs)
    runner.programs.value
    start_run_time = time()
    spark_score = runner.run(record_stats=True)
    end_time = time()
    with open(output, mode='a+') as out:
        if record_iterations:
            out.write('{}\n'.format(json.dumps({
                                'type': 'iteration',
                                'iterations_lfi': lfi.iteration,
                                'score_lfi': lfi_score,
                                # 'weights_lfi': str(lfi._weights),
                                'iterations_dlfi': runner.total_iterations,
                                'score_dlfi': spark_score,
                                # 'weights_dlfi': runner.weights,
                                'num_examples': len(examples),
                                'program': program.source,
                                'num_unknowns': lfi.count,
                                'num_parallel': len(runner.valid_parameter_programs)})))
        if record_time:
            out.write('{}\n'.format(json.dumps({
                                'type': 'time',
                                'total_time_lfi': end_local_time - start_local_time,
                                'total_time_dlfi': end_time - start_time,
                                'compilation_time_dlfi': start_run_time - start_time,
                                'run_time_dlfi': end_time - start_run_time,
                                'em_time_dlfi': runner.total_time,
                                'spark_run_overhead_dlfi': end_time - start_run_time - runner.total_time,
                                'score_lfi': lfi_score,
                                'score_dlfi': spark_score,
                                'program': program.source,
                                'num_examples': len(examples),
                                'num_unknowns': lfi.count,
                                'num_parallel': len(runner.valid_parameter_programs)})))

def benchmark_dlfi_max_iterations_hyperparameter(program, examples, context, output, params):
    max_iterations = int(params[0])
    runner = dlfi.SparkRunner(context, max_iterations=500)
    runner.compile(examples, program, max_iter=max_iterations)
    start_time = time()
    runner.run(record_stats=True)
    end_time = time()
    with open(output, 'a+') as f:
        json.dump({
            'type': 'max_iterations',
            'program': program.source,
            'num_parameters': runner.general_problem.count,
            'num_examples': len(examples),
            'total_iterations': runner.total_iterations,
            'total_time': runner.total_time,
            'waiting_time': end_time - start_time,
            'max_iterations': max_iterations
        }, f)
        f.write('\n')

def run_benchmark(benchmark, file, context, output, drop_evidence=0.5, *args, **kwargs):
    with open(file) as instrumentation:
        previous_program = None
        for line in instrumentation:
            print(line)
            completed = False
            while not completed:
                if line == '' or line.startswith('#'):
                    continue
                split = line.split()
                source = split[0]
                reuse_program = source == '"'
                examples = list(dlfi.read_examples([split[1]], drop_evidence=drop_evidence))[:int(split[2])]
                if reuse_program:
                    program = previous_program
                else:
                    program = BenchmarkPrologFile(source, additional_unknowns=int(split[3]) if len(split) > 3 else 0)
                params = split[4:]
                try:
                    benchmark(program, examples, context, output, params, *args, **kwargs)
                    completed = True
                except (KeyboardInterrupt, SystemExit):
                    raise
                except:
                    print('Found error but repeating: {}'.format(sys.exc_info()[0]))
            if not reuse_program and not previous_program is None:
                previous_program.remove()
            previous_program = program

class BenchmarkPrologFile(PrologFile):

    def __init__(self, filename, parser=None, factory=None, identifier=0,
                    additional_unknowns=0):
        from random import shuffle, random
        from os import linesep

        self.source = filename
        with open(filename) as f:
            original_source_text = f.read()

        source_text = ''
        remaining = additional_unknowns
        clauses = original_source_text.split('.' + linesep)
        shuffle(clauses)
        disjunct_count = []
        for clause in clauses:
            if clause != '':
                disjuncts = clause.split(';')
                disjunct_count.append(len(disjuncts))
                if len(disjuncts) <= remaining:
                    t = [random() for i in range(len(disjuncts))]
                    norm = sum(t) + random() # Normalization includes a dummy value
                    t_norm = [t_val / norm for t_val in t]
                    for i in range(len(disjuncts)):
                        disjunct = disjuncts[i]
                        if not '::' in disjunct:
                            source_text += disjunct
                        else:
                            parts = disjunct.split('::')
                            source_text += 't({}) :: {}'.format(t_norm[i], parts[1])
                            remaining -= 1
                        if i < len(disjuncts) - 1:
                            source_text += ';'
                else:
                    source_text += clause
                source_text += '.' + linesep
        if remaining > 0:
            if BenchmarkPrologFile._can_find(disjunct_count, additional_unknowns):
                BenchmarkPrologFile.__init__(self, filename, parser, factory, identifier, additional_unknowns)
            else:
                raise ValueError('Cannot select exactly {} parameters'.format(additional_unknowns))
        else:
            self.source_text = source_text
            self.tmpfile = filename + '.tmp'
            with open(self.tmpfile, mode='w+') as tmp:
                tmp.write(source_text)
                
            PrologFile.__init__(self, self.tmpfile, parser, factory, identifier)

    def remove(self):
        from os import remove
        try:
            try:
                remove(self.tmpfile)
            except OSError:
                pass
            del self.tmpfile
        except AttributeError:
            pass

    def __del__(self):
        self.remove()

    @staticmethod
    def _can_find(counts, num):
        for idx in range(len(counts)):
            val = counts[idx]
            if counts[idx] == num:
                return True
            elif idx < num and BenchmarkPrologFile._can_find([counts[i] for i in range(len(counts)) if i != idx], num - val):
                return True
        return False

def take_without_replacement(n, lst):
    if len(lst) < n:
        raise ValueError('Cannot take {} elements from a list with {} elements without replacement'.format(n, len(lst)))
    from random import randint
    random_list = []

    for _ in range(n):
        idx = randint(0, len(lst) - 1)
        random_list.append(lst[idx])
        del lst[idx]
    return random_list

def argparser():
    import argparse
    parser = argparse.ArgumentParser(description='Benchmarking learning from interpretations in Spark')
    parser.add_argument('master', help='The master of the Spark cluster the application should run on')
    parser.add_argument('output')
    parser.add_argument('--drop-evidence', default=0.0, dest='drop_evidence', type=float,
                        help='The probability that a single line of evidence is ignored')
    parser.add_argument('--iterations', default=None, dest='iteration_benchmark',
                        help='Perform a benchmark that counts the number of iterations performed in the learning task for the specified runs')
    parser.add_argument('--time', default=None, dest='time_benchmark',
                        help='Perform a benchmark that times several phases in the parameter learning implementation of Spark')
    parser.add_argument('--compile', default=None, dest='compile',
                        help='Record the compilation of the provided examples and the provided model in LFI and DLFI')
    parser.add_argument('--max_iterations', default=None, dest='max_iterations_benchmark',
                        help='Record the time and number of iterations spend when changing the maximum number of local iterations')
    return parser


def main(argv):
    parser = argparser()
    args = parser.parse_args(argv)
    context = dlfi.connect_to_spark_master(args.master)
    if not args.compile is None:
        run_benchmark(benchmark_dlfi_compilation, args.compile, context, args.output)
    if not args.time_benchmark is None or not args.iteration_benchmark is None:
        time_benchmark = False if args.time_benchmark is None else args.time_benchmark
        iteration_benchmark = False if args.iteration_benchmark is None else args.iteration_benchmark
        run_benchmark(benchmark_dlfi_number_iterations, args.iteration_benchmark or args.time_benchmark, context, args.output, spark_max_iterations=1e10, dlfi_kwargs={'max_iter': 2}, lfi_kwargs={'max_iter': 1e10}, record_time=time_benchmark, record_iterations=iteration_benchmark)
    if not args.max_iterations_benchmark is None:
        run_benchmark(benchmark_dlfi_max_iterations_hyperparameter, args.max_iterations_benchmark, context, args.output)

if __name__ == '__main__':
    main(sys.argv[1:])
