person(john).
person(mary).
person(robert).
person(avril).
person(pedro).
person(raguel).
person(santina).
person(julietta).
person(eddy).
person(hye).
person(patricia).
person(lacy).
person(herminia).
person(erick).
person(ebonie).
person(craig).
person(orlando).
person(natalia).
person(catarina).
person(micheline).
person(felicita).
person(jero).

t(_)::burglary.
t(_)::earthquake.

t(_)::alarm :- burglary, earthquake.
t(_)::alarm :- burglary, \+earthquake.
t(_)::alarm :- \+burglary, earthquake.

t(_)::calls(X) :- alarm, person(X).
t(_)::calls(X) :- \+alarm, person(X).
